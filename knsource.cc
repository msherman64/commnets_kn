#include <click/config.h>
#include <click/args.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/string.hh>
#include "knsource.hh"
#include "kn_packets.hh"
CLICK_DECLS
//addedcomment
KnSource::KnSource() : _timer(this),_k(1),_dest1(0),_dest2(0),_dest3(0) {}
KnSource::~KnSource(){}

int KnSource::configure(Vector<String> &conf, ErrorHandler *errh) {
    return Args(conf, this, errh)
        .read_m("TYPE", _type)
        .read_m("SOURCE", _source)
        .read_m("SEQNUM", _seqnum)
        .read("K", _k)
        .read("DEST1", _dest1)
        .read("DEST2", _dest2)
        .read("DEST3", _dest3)
        .read("PAYLOAD", _payload)
        .complete();
}

int KnSource::initialize(ErrorHandler *) {
    _timer.initialize(this);
    _timer.schedule_after_sec(3);
    return 0;
}

void KnSource::push(int port, Packet *p) {
    output(port).push(p);
    return;	
}

void KnSource::run_timer(Timer *timer){

    push(0, makePacket());
    _timer.reschedule_after_sec(3);

}

Packet* KnSource::makePacket() {
    WritablePacket *basepacket = Packet::make(0);

    switch(_type){
        case 1 : { //already done all hello tasks
                     //WritablePacket *packet = Packet::make(0, 0, sizeof(hello_packet_format), 0); //make packet with payload
                     WritablePacket *packet = basepacket->push(sizeof(hello_packet_format));
                     hello_packet_format *format = (hello_packet_format *)packet->data(); //cast packet to struct
                     format->type = _type; //set length based on payload
                     format->source = _source;
                     format->seqnum = _seqnum;
                 }
                 break;
        case 2 : ;//update not yet implemented
                 break;
        case 3 : {
                     //WritablePacket *packet = Packet::make(0, 0, sizeof(ack_packet_format), 0); //make packet with payload
                     WritablePacket *packet = basepacket->push(sizeof(ack_packet_format));
                     ack_packet_format *format = (ack_packet_format *)packet->data(); //cast packet to struct
                     format->type = _type; //set length based on payload
                     format->source = _source;
                     format->seqnum = _seqnum;
                     format->dest = _dest1;
                 }
                 break;
        case 4 : {
                     uint16_t payload_length=0;
                     WritablePacket *packet;
                     if(!_payload.empty()){
                         WritablePacket *payload = Packet::make(sizeof(data_packet_format),_payload.data(),_payload.length(), 0); //make packet with payload
                         packet = payload->push(sizeof(data_packet_format));
                         payload_length=_payload.length();
                     }
                     else{
                         packet = basepacket->push(sizeof(data_packet_format));
                     }
                     data_packet_format *format = (data_packet_format *)packet->data(); //cast packet to struct
                     format->type = _type; //set length based on payload
                     format->source = _source;
                     format->seqnum = _seqnum;
                     format->k = _k;
                     format->dest1 = _dest1;
                     format->dest2 = _dest2;
                     format->dest3 = _dest3;
                     format->payload_length=payload_length;
                     return packet;
                 }
                 break;
    }
    //	WritablePacket *packet = packetp->push(sizeof(hello_packet_format)); //push, insert space for header in front, data pointer points to front of our custom header
    return basepacket;

}

    CLICK_ENDDECLS
EXPORT_ELEMENT(KnSource)
