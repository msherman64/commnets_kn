#ifndef CLICK_KNCLASSIFIER_HH
#define CLICK_KNCLASSIFIER_HH

#include <click/element.hh>

CLICK_DECLS

class KnClassifier : public Element {

public:
         KnClassifier();
	~KnClassifier();
	
	const char *class_name() const { return "KnClassifier";}
	const char *port_count() const { return "1-/2"; }
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);

	void push(int, Packet *);

};

CLICK_ENDDECLS

#endif

