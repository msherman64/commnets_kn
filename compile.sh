#! /bin/bash


cp ./knclassifier.cc ~/click/elements/local/knclassifier.cc
cp ./knclassifier.hh ~/click/elements/local/knclassifier.hh

cp ./knsource.cc ~/click/elements/local/knsource.cc
cp ./knsource.hh ~/click/elements/local/knsource.hh

cp ./knswitch.cc ~/click/elements/local/knswitch.cc
cp ./knswitch.hh ~/click/elements/local/knswitch.hh

cp ./knroute.cc ~/click/elements/local/knroute.cc
cp ./knroute.hh ~/click/elements/local/knroute.hh

cp ./knupdate.cc ~/click/elements/local/knupdate.cc
cp ./knupdate.hh ~/click/elements/local/knupdate.hh

cp ./knforward.cc ~/click/elements/local/knforward.cc
cp ./knforward.hh ~/click/elements/local/knforward.hh

cp ./knarq.cc ~/click/elements/local/knarq.cc
cp ./knarq.hh ~/click/elements/local/knarq.hh

cp ./kn_packets.hh ~/click/elements/local/kn_packets.hh

# Can edit in place
cp ./elements/kn_router.click ~/elements/kn_router.click
cp ./elements/kn_client.click ~/elements/kn_client.click
	
cd ~/click/
make elemlist
make
