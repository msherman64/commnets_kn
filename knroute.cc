#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet_anno.hh>
#include "knroute.hh"
#include "kn_packets.hh"
#include <vector>

CLICK_DECLS

KnRoute::KnRoute(): _rtable(){}
KnRoute::~KnRoute(){}

int KnRoute::configure(Vector<String> &conf, ErrorHandler *errh) {
    bool parsed = Args(conf, this, errh)
        .read_m("ADDRESS", _address)
        .complete();
    if(parsed==0){
        init_routes(_address);
        return 0; //parsing succeeded, added self to routing table
    }
    return -1;//parsing failed
}

void KnRoute::push(int port, Packet *p) {
    hello_packet_format *hello = (hello_packet_format *)(p->data()); //hello has fewest asumptions, only need type
    //click_chatter("message type %d", hello->type);

    switch(hello->type){
        case 1:
        case 2: neighbor_table(p); //hello
                 routing_table(p); //update
                 break;
        default : click_chatter("invalid type, classifier failed");
                  break;
    }
}

void KnRoute::init_routes(uint16_t address){
    rt_val init_self;
    init_self.cost = 0;
    init_self.nexthop = 0;
    init_self.nexthop2 = 0;
    init_self.nexthop3 = 0;
    _rtable.set(address,init_self); //addr not in table, added

}

void KnRoute::neighbor_table(Packet *p){
    hello_packet_format *hello = (hello_packet_format *)(p->data()); //cast packet
    int source_addr = hello->source; //get source addr
    int in_port = p->anno_u8(1); //get input port annotation
    bool added_n = _ntable.set(source_addr,in_port); //add/replace source, port pair to neighbor table
    if(added_n){click_chatter("is new neighbor table entry? %d \n", added_n);}
}


void KnRoute::routing_table(Packet *p)
{
    hello_packet_format *hello = (hello_packet_format *)(p->data()); //cast packet
    uint16_t source_addr = hello->source;
    std::vector<process_val> update_vec;

    // add adjacent node to routing table
    process_val hello_rt;
    hello_rt.cost = 1;
    hello_rt.dest = source_addr;
    hello_rt.nexthop = source_addr;
    hello_rt.nexthop2 = 0;
    hello_rt.nexthop3 = 0;
    update_vec.push_back(hello_rt);

    uint16_t payload_length=0;
    if(hello->type==2)
    {
        update_packet_format *update = (update_packet_format *)(p->data()); //cast packet to update struct
        payload_length=update->payload_length;

    }
    
    if(payload_length)
    { //check non-empty payload
//        click_chatter("UPDATE payload length is %d",payload_length);
//        click_chatter("UPDATE field size is %d",sizeof(update_val));
//        click_chatter("length is multiple of field size if 0=%d",payload_length%sizeof(update_val));
//        click_chatter("Number of routes in update is %d",payload_length/sizeof(update_val));
        if((payload_length%sizeof(update_val))==0)
        { //check that payload is multiple of entry size
            p->pull(sizeof(update_packet_format)); //discard header
            update_val *payload = (update_val *)(p->data()); //get pointer to first value

            int entries = payload_length/sizeof(update_val); //calculate num entriesin packet
            for(int i=0; i<entries; i++)
            { 
                //iterate across entries
                //click_chatter("UPDATE address %d, cost %d",payload[i].addr,payload[i].cost);
                process_val update_rt; //temporary value
                update_rt.cost = payload[i].cost; //cost is cost from payload
                update_rt.dest = payload[i].addr; //key is address from payload
                update_rt.nexthop = source_addr; //nexthop is source of update
                update_rt.nexthop2 = 0; //init extra entries
                update_rt.nexthop3 = 0; 
                update_rt.cost +=1; //received from neighbor, so one extra hop
                update_vec.push_back(update_rt);
    //            click_chatter("adding nexthop %d for destination %d",update_rt.nexthop,update_rt.dest);
            }
        }
    }

    for(std::vector<process_val>::iterator it = update_vec.begin(); it!=update_vec.end();it++)
    {
        uint16_t dest_addr = it->dest;
        rt_val update_rt;
        update_rt.cost=it->cost;
        update_rt.nexthop=it->nexthop;
        update_rt.nexthop2=it->nexthop2;
        update_rt.nexthop3=it->nexthop3;

        Rtable::iterator rtable_it = _rtable.find(dest_addr); //check for value
        if(rtable_it == _rtable.end()){
            _rtable.set(dest_addr,update_rt); //addr not in table, added
            click_chatter("addr not in table, adding");
        }
        else if(update_rt.cost < rtable_it.value().cost){
            _rtable.set(dest_addr,update_rt); //shorter route
            click_chatter("shorter route found, adding");
        }
        else if(update_rt.cost == rtable_it.value().cost)
        {
            uint16_t nexthop = source_addr;
            update_rt = rtable_it.value();
            if((nexthop!=update_rt.nexthop)&&(nexthop!=update_rt.nexthop2)&&(nexthop!=update_rt.nexthop3))
            {
                if (update_rt.nexthop==0){
                    update_rt.nexthop=nexthop;
                    _rtable.set(dest_addr,update_rt);
                    click_chatter("shouldn't happen, dest with no nexthop was in table");
                }
                else if(update_rt.nexthop2==0){
                    update_rt.nexthop2=nexthop;
                    _rtable.set(dest_addr,update_rt);
                    click_chatter("added second route to dest %d",dest_addr);
                }
                else if(update_rt.nexthop3==0){         //check for empty values
                    update_rt.nexthop3=nexthop;
                    _rtable.set(dest_addr,update_rt); //new nexthop
                    click_chatter("added third route to dest %d",dest_addr);
                }
               // else { click_chatter("have 3 routes already");}
            }
          //  else{click_chatter("already a nexthop"); }
        }
      //  else{ click_chatter("route not shorter"); }
    }
}

    CLICK_ENDDECLS
EXPORT_ELEMENT(KnRoute)
