#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet_anno.hh>
#include "knupdate.hh"
#include "kn_packets.hh"

CLICK_DECLS

KnUpdate::KnUpdate() : _timer(this),_timeout(5) {}
KnUpdate::~KnUpdate(){}

int KnUpdate::configure(Vector<String> &conf, ErrorHandler *errh) {

    String ename;

return Args(conf, this, errh)
    .read_m("ADDRESS", _address)
    .read_m("TIMEOUT", _timeout)
    .read_m("ROUTING", _ename)
    .complete();
}

int KnUpdate::initialize(ErrorHandler *errh) {

    int rtret=1;
    if(Element* e = cp_element(_ename, this, errh)){
//        click_chatter("found routing table element");
        _rt_elem = (KnRoute *)(e); //cast element to router type
        rtret=0; //indicate success
    }
    _timer.initialize(this);
    _timer.schedule_after_sec(_timeout);
    return rtret;
}

void KnUpdate::push(int port, Packet *p) {
    click_chatter("received erronious packet on %d",port);
    p->kill();  //should not happen
}

void KnUpdate::read_table(){
    _rtable = _rt_elem->get_rtable();
    int size = _rtable.size();
//    click_chatter("table has %d addresses \n", size);
    
    Update_Payload payload[size]; //initialize payload to num rows

    if(size){ //if has entries
        int i=0; //init array idex
        for( KnRoute::Rtable::iterator rtable_it = _rtable.begin();
                rtable_it != _rtable.end();
                rtable_it++)
        {
            uint16_t addr = rtable_it.key();
            KnRoute::rt_val itrval = rtable_it.value();
            click_chatter("address %d has cost %d and nexthop %d",addr,itrval.cost,itrval.nexthop);
            //update packet payload is pairs of addr(uint16_t),cost(uint8_t)
            payload[i] = {addr,itrval.cost}; //write table entry into payload
            i++;
        }
   }

    uint16_t payload_size = sizeof(payload);
//    click_chatter("payload size %d",payload_size);

    WritablePacket *p = Packet::make(sizeof(update_packet_format),payload,payload_size,0);
    WritablePacket *p_header = p->push(sizeof(update_packet_format));
    update_packet_format *update = (update_packet_format *)(p_header->data()); //cast packet
    update->type=2;
    update->source=_address;
    update->seqnum=0;
    update->payload_length = payload_size;

    p_header->set_anno_u8(2, 255); //let 255 or FF mean all ports

    output(0).push(p_header);

}


void KnUpdate::run_timer(Timer *){
    read_table();
    _timer.schedule_after_sec(_timeout);
}


CLICK_ENDDECLS
EXPORT_ELEMENT(KnUpdate)

