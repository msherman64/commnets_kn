#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet_anno.hh>
#include "knclassifier.hh"
#include "kn_packets.hh"

CLICK_DECLS

KnClassifier::KnClassifier(){}
KnClassifier::~KnClassifier(){}

int KnClassifier::configure(Vector<String> &conf, ErrorHandler *errh) {
    return 0;
}

void KnClassifier::push(int port, Packet *p) {
    hello_packet_format *cHdr = (hello_packet_format *)(p->data()); //hello has fewest asumptions, only need type

//    click_chatter("received packet on %d",port);

    p->set_anno_u8(1, port); //annotate packet with source port in annotation byte 1

    switch(cHdr->type) {
    case 1 : output(0).push(p); //hello, send to routing
            break;
    case 2 : output(0).push(p); //update, send to routing
            break;
    case 3 : p->kill();  //ack
             click_chatter("Error, received ACK"); //ack should not pass reliability element
            break;
    case 4 : output(1).push(p);  //data, send to forwarding
            break;
    default : p->kill();  //other
             click_chatter("Error, received invalid type"); //catch all
            break;
    }
}

CLICK_ENDDECLS
EXPORT_ELEMENT(KnClassifier)

