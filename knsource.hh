#ifndef CLICK_KNSOURCE_HH
#define CLICK_KNSOURCE_HH

#include <click/element.hh>
#include <click/timer.hh>

CLICK_DECLS

class KnSource : public Element {

public:
	KnSource();
	~KnSource();
	
	const char *class_name() const { return "KnSource";}
	const char *port_count() const { return "0/1"; }
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);
	int initialize(ErrorHandler*);
	void push(int, Packet *);
	Packet* makePacket();
	
	void run_timer(Timer*);
	
private:
	Timer _timer;

// fields for packet init
    uint8_t _type;
    uint16_t _source;
    uint8_t _seqnum;
    uint16_t _length;
    uint8_t _k;
    uint16_t _dest1;
    uint16_t _dest2;
    uint16_t _dest3;
    String _payload;



};

CLICK_ENDDECLS

#endif
