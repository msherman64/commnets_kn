#ifndef CLICK_KNARQ_HH
#define CLICK_KNARQ_HH

#include <click/element.hh>
#include <click/timer.hh>
#include <click/task.hh>

CLICK_DECLS

class KnArq : public Element {

public:
	KnArq();
	~KnArq();
	
	const char *class_name() const { return "KnArq";}
	const char *port_count() const { return "2/2"; }
	const char *processing() const { return "hl/hh"; }
// input 0 push from routerport
// input 1 pull from switch
// output 0 push to routerport
// output 1 push to classifier	
	int configure(Vector<String>&, ErrorHandler*);
	int initialize(ErrorHandler*);
	void push(int, Packet *);
	
	void run_timer(Timer*);
	bool run_task(Task*);
	void send_ack(Packet *);
	void ack_handler(Packet *);
	void message_handler(Packet *);
    bool pull_message();
	void send_message();
	
private:
	Timer _timer;
	Task _pulltask;

// fields for packet init
    uint8_t _timeout;
    uint8_t _last_recv_seqnum;
    uint8_t _last_msg_sent;
    Packet* _qpacket;
    bool _acked;
};

CLICK_ENDDECLS

#endif
