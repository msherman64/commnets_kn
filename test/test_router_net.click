require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library /home/comnetsii/elements/kn_router.click);


router1::Kn_Router(NETADDRESS 1)
router2::Kn_Router(NETADDRESS 2)
router3::Kn_Router(NETADDRESS 3)

port1::RouterPort(DEV $dev1, IN_MAC $mac1, OUT_MAC $mac2)
port2::RouterPort(DEV $dev2, IN_MAC $mac2, OUT_MAC $mac1)
port3::RouterPort(DEV $dev3, IN_MAC $mac3, OUT_MAC $mac4)
port4::RouterPort(DEV $dev4, IN_MAC $mac4, OUT_MAC $mac3)

port1
->[0]router1[0]
->port1;

port2
->[0]router2[0]
->port2;

port3
->[1]router2[1]
->port3;

port4
->[0]router3[0]
->port4;
