source0::KnSource(TYPE 4, SOURCE 123, SEQNUM 0, K 2, DEST1 456, DEST2 789, DEST3 79)
source1::KnSource(TYPE 1, SOURCE 456, SEQNUM 0)
source2::KnSource(TYPE 1, SOURCE 789, SEQNUM 0)
source3::KnSource(TYPE 1, SOURCE 79, SEQNUM 0)
switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS 5)
update::KnUpdate(ADDRESS 5, TIMEOUT 1, ROUTING route)
forward::KnForward(ROUTING route)

source0
->Print("DATA SOURCE")
-> [0]classifier;
source1
-> [1]classifier;
source2
-> [2]classifier;
source3
-> [3]classifier;

classifier[0]
-> route;

classifier[1]
-> forward
-> [1]switch;

update
-> [0]switch;

switch[0]
-> Print("switch output 0")
-> Discard();

switch[1]
-> Print("switch output 1")
-> Discard();

switch[2]
-> Print("switch output 2")
-> Discard();

switch[3]
-> Print("switch output 3")
-> Discard();
