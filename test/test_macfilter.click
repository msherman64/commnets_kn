require(library /home/comnetsii/elements/lossyrouterport.click);

port1::LossyRouterPort(DEV veth1, IN_MAC 11:11:11:11:11:11, OUT_MAC 22:22:22:22:22:22, LOSS 0.99 , DELAY 0.2)
port2::LossyRouterPort(DEV veth2, IN_MAC 22:22:22:22:22:22, OUT_MAC 11:11:11:11:11:11, LOSS 0.99 , DELAY 0.2)

source1::InfiniteSource()
source2::InfiniteSource()

source1-> port1->Print("from port 1")->Discard;

source2-> port2 ->Print("from port 2")->Discard;
