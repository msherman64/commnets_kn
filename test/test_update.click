source0::KnSource(TYPE 1, SOURCE 1, SEQNUM 0)
source1::KnSource(TYPE 1, SOURCE 2, SEQNUM 0)
source2::KnSource(TYPE 1, SOURCE 3, SEQNUM 0)
source3::KnSource(TYPE 1, SOURCE 4, SEQNUM 0)

classifier::KnClassifier()
route::KnRoute(ADDRESS 5)
update::KnUpdate(ADDRESS 5, TIMEOUT 1, ROUTING route)

source0
-> [0]classifier;
source1
-> [1]classifier;
source2
-> [2]classifier;
source3
-> [3]classifier;

classifier[0]
-> Print("Sent to routing")
-> route;

update
-> Print("Update Message")
-> Discard();

classifier[1]
-> Print("Sent to forwarding")
-> Discard();

