source1::KnSource(TYPE 1)
source2::KnSource(TYPE 2)
source3::KnSource(TYPE 3)
source4::KnSource(TYPE 4)
classifier::KnClassifier()
switch::KnSwitch()


source1
-> [0]classifier;

source2
-> [1]classifier;

source3
-> [2]classifier;

source4
-> [3]classifier;

classifier[0]
-> Print("Sent to routing")
-> [0]switch[0]
-> Discard();

classifier[1]
-> Print("Sent to forwarding")
-> [1]switch[1]
-> Discard();

