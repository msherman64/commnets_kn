source1::KnSource(TYPE 1, SOURCE 1, SEQNUM 0)
source2::KnSource(TYPE 1, SOURCE 2, SEQNUM 0)
source3::KnSource(TYPE 1, SOURCE 3, SEQNUM 0)

classifier::KnClassifier()
route::KnRoute(ADDRESS 4)

source1
-> [0]classifier;

source2
-> [1]classifier;

source3
-> [2]classifier;

classifier[0]
-> Print("Sent to routing")
-> route
-> Discard();

classifier[1]
-> Print("Sent to forwarding")
-> Discard();

