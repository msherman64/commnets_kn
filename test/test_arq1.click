require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);

port1::LossyRouterPort(DEV $dev1, IN_MAC $mac1, OUT_MAC $mac2, LOSS 1    , DELAY 1)

source1::KnSource(TYPE 4,SOURCE 1, SEQNUM 0)
arq1::KnArq(TIMEOUT 5)

arq1[0]
->Print("arq1 to port")
-> port1
->Print("arq1 from port")
-> [0]arq1;

source1
->Print("new message")
->Queue()
->Print("arq1 from queue")
->[1]arq1[1]
->Print("arq1 to classifier")
->Discard();
