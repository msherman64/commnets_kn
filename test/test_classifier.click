source1::KnSource(TYPE 1)
source2::KnSource(TYPE 2)
source3::KnSource(TYPE 3)
source4::KnSource(TYPE 4)
classifier::KnClassifier()

source1
-> [0]classifier;

source2
-> [1]classifier;

source3
-> [2]classifier;

source4
-> [3]classifier;

classifier[0]
-> Print("Sent to routing")
-> Discard();

classifier[1]
-> Print("Sent to forwarding")
-> Discard();

