require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);

port1::LossyRouterPort(DEV $dev1, IN_MAC $mac1, OUT_MAC $mac2, LOSS 0.9,DELAY 0.1)
port2::LossyRouterPort(DEV $dev2, IN_MAC $mac2, OUT_MAC $mac1, LOSS 0.9,DELAY 0.1)

source1::KnSource(TYPE 4,SOURCE 1, SEQNUM 0)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)

arq1[0]
->Print("arq1 to port")
-> port1
->Print("arq1 from port")
-> [0]arq1;

arq2[0]
->Print("arq2 to port")
-> port2
->Print("arq2 from port")
-> [0]arq2;

source1
->Print("new message")
->Queue()
->[1]arq1[1]
->Print("arq1 to classifier")
->Discard();

Idle()
->Queue()
->[1]arq2[1]
->Print("arq2 to classifier")
->Discard();


