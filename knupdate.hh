#ifndef CLICK_KNUPDATE_HH
#define CLICK_KNUPDATE_HH

#include <click/element.hh>
#include <click/timer.hh>
#include <click/hashtable.hh>
#include <click/list.hh>
#include "knroute.hh"
CLICK_DECLS

class KnUpdate : public Element {

public:
    KnUpdate();
	~KnUpdate();
	
	const char *class_name() const { return "KnUpdate";}
	const char *port_count() const { return "0/1"; }
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);
    int initialize(ErrorHandler*);

	void push(int, Packet *);

    void run_timer(Timer *);

#pragma pack(push,1)
struct Update_Payload
    {
        uint16_t address;
        uint8_t cost;
    };
#pragma pack(pop)

void read_table();
//	void send_update(Update_Payload);

private:
    String _ename;
    Timer _timer;
    uint8_t _timeout;
    uint16_t _address;


    KnRoute * _rt_elem;
    KnRoute::Ntable _ntable;
    KnRoute::Rtable _rtable;

};

CLICK_ENDDECLS

#endif

