#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet_anno.hh>
#include "knswitch.hh"
#include "kn_packets.hh"

CLICK_DECLS

KnSwitch::KnSwitch(){}
KnSwitch::~KnSwitch(){}

int KnSwitch::configure(Vector<String> &conf, ErrorHandler *errh) {
    return 0;
}

void KnSwitch::push(int port, Packet *p) {
    //    hello_packet_format *cHdr = (hello_packet_format *)(p->data()); //hello has fewest asumptions, only need type

//    int in_port=p->anno_u8(1); //get annotation for router input port, position1
    int out_port=p->anno_u8(2); //get annotation for output port, position2
//    click_chatter("SWITCH output annotation = %d",out_port);

    if (out_port==255){ //check for broadcast flag
        for (int i = 0; i < noutputs(); i++)
        {
            output(i).push(p->clone());
        }
        p->kill(); //cleanup last packet

        //    click_chatter("broadcasting");
    }
    else
        output(out_port).push(p); //send out designated out port




    //    click_chatter("router received packet on %d",in_port);
    //    click_chatter("switch received packet on %d",port);
    //    click_chatter("switch sending packet on %d",out_port);



}





    CLICK_ENDDECLS
EXPORT_ELEMENT(KnSwitch)

