#ifndef CLICK_KNROUTE_HH
#define CLICK_KNROUTE_HH

#include <click/element.hh>
#include <click/hashtable.hh>
#include <click/list.hh>
CLICK_DECLS

class KnRoute : public Element {

public:
    KnRoute();
	~KnRoute();
	
	const char *class_name() const { return "KnRoute";}
	const char *port_count() const { return "1/0"; }
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);

	void push(int, Packet *);
	void init_routes(uint16_t);
	void neighbor_table(Packet *);
	void routing_table(Packet *);
#pragma pack(push,1)
    struct update_val{
        uint16_t addr;
        uint8_t cost;
    };

    struct rt_val{
        uint16_t nexthop;
        uint16_t nexthop2;
        uint16_t nexthop3;
        uint8_t cost;
    };

    struct process_val{
        uint16_t dest;
        uint16_t nexthop;
        uint16_t nexthop2;
        uint16_t nexthop3;
        uint8_t cost;
    };
#pragma pack(pop)
    typedef HashTable<uint16_t,uint8_t> Ntable;
    typedef HashTable<uint16_t,rt_val> Rtable;

//    KnRoute::Rtable const & get_rtable() const { return _rtable; }
    KnRoute::Rtable get_rtable() { return _rtable; }
    KnRoute::Ntable get_ntable() { return _ntable; }

private:

    uint16_t _address;


    Ntable _ntable;
    Rtable _rtable;

};

CLICK_ENDDECLS

#endif

