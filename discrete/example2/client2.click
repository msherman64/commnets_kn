require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library /home/comnetsii/elements/kn_client.click);

client::Kn_Client(NETADDRESS 2)
//client::Kn_Client(NETADDRESS 1, K 1, DEST1 2)
//client::Kn_Client(NETADDRESS 1, K 2, DEST1 2, DEST2 3)
//client::Kn_Client(NETADDRESS 1, K 3, DEST1 2, DEST2 3, DEST3 4)

port::LossyRouterPort(DEV veth20, IN_MAC 96:85:28:25:f8:a2, OUT_MAC 46:f9:e8:5d:46:93, DELAY 0.2, LOSS 0.9)
port->[0]client[0]->port;



//Copy template as needed, uncomment only appropriate client line.
//Paste in device and address as needed
/*
DEV veth1, IN_MAC 9e:79:52:60:48:7d, OUT_MAC 36:32:83:71:a9:1c
DEV veth2, IN_MAC 36:32:83:71:a9:1c, OUT_MAC 9e:79:52:60:48:7d
DEV veth3, IN_MAC 26:85:da:2e:5d:33, OUT_MAC 16:1e:c8:49:57:8b
DEV veth4, IN_MAC 16:1e:c8:49:57:8b, OUT_MAC 26:85:da:2e:5d:33
DEV veth5, IN_MAC 6a:ae:18:a3:53:21, OUT_MAC 66:6d:03:6a:74:f9
DEV veth6, IN_MAC 66:6d:03:6a:74:f9, OUT_MAC 6a:ae:18:a3:53:21
DEV veth7, IN_MAC 4a:b3:a1:04:cb:44, OUT_MAC da:f3:a4:50:c8:3a
DEV veth8, IN_MAC da:f3:a4:50:c8:3a, OUT_MAC 4a:b3:a1:04:cb:44
DEV veth9, IN_MAC 82:23:7b:91:96:5c, OUT_MAC 16:0c:35:21:2c:b2
DEV veth10, IN_MAC 16:0c:35:21:2c:b2, OUT_MAC 82:23:7b:91:96:5c
DEV veth11, IN_MAC 52:09:6a:77:cb:a4, OUT_MAC 9a:10:2e:8e:35:6a
DEV veth12, IN_MAC 9a:10:2e:8e:35:6a, OUT_MAC 52:09:6a:77:cb:a4
DEV veth13, IN_MAC 52:8d:e7:86:3c:81, OUT_MAC 62:16:db:71:e8:b1
DEV veth14, IN_MAC 62:16:db:71:e8:b1, OUT_MAC 52:8d:e7:86:3c:81
DEV veth15, IN_MAC b2:65:e8:03:25:0a, OUT_MAC f6:25:40:fe:71:4d
DEV veth16, IN_MAC f6:25:40:fe:71:4d, OUT_MAC b2:65:e8:03:25:0a
DEV veth17, IN_MAC 3e:7e:22:70:c5:5e, OUT_MAC 86:6e:3f:ea:c8:20
DEV veth18, IN_MAC 86:6e:3f:ea:c8:20, OUT_MAC 3e:7e:22:70:c5:5e
DEV veth19, IN_MAC 46:f9:e8:5d:46:93, OUT_MAC 96:85:28:25:f8:a2
DEV veth20, IN_MAC 96:85:28:25:f8:a2, OUT_MAC 46:f9:e8:5d:46:93
*/
