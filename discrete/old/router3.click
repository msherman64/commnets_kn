require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library ../elements/kn_router.click);

router::Kn_Router(NETADDRESS 3)

port1::RouterPort(DEV $dev1, IN_MAC $inmac1, OUT_MAC $outmac1)
port2::RouterPort(DEV $dev1, IN_MAC $inmac2, OUT_MAC $outmac2)
port3::RouterPort(DEV $dev1, IN_MAC $inmac3, OUT_MAC $outmac3)

port1->[0]router[0]->port1;
port2->[1]router[1]->port2;
port3->[2]router[2]->port3;
