require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library ../elements/kn_router.click);

router::Kn_Router(NETADDRESS 3)

port1::LossyRouterPort(DEV $dev1, IN_MAC $inmac1, OUT_MAC $outmac1 ,DELAY 0.2, LOSS 0.9)
port2::LossyRouterPort(DEV $dev2, IN_MAC $inmac2, OUT_MAC $outmac2 ,DELAY 0.2, LOSS 0.9)
port3::LossyRouterPort(DEV $dev3, IN_MAC $inmac3, OUT_MAC $outmac3 ,DELAY 0.2, LOSS 0.9)
port4::LossyRouterPort(DEV $dev4, IN_MAC $inmac4, OUT_MAC $outmac4 ,DELAY 0.2, LOSS 0.9)
port5::LossyRouterPort(DEV $dev5, IN_MAC $inmac5, OUT_MAC $outmac5 ,DELAY 0.2, LOSS 0.9)
port6::LossyRouterPort(DEV $dev6, IN_MAC $inmac6, OUT_MAC $outmac6 ,DELAY 0.2, LOSS 0.9)
port7::LossyRouterPort(DEV $dev7, IN_MAC $inmac7, OUT_MAC $outmac7 ,DELAY 0.2, LOSS 0.9)
port8::LossyRouterPort(DEV $dev8, IN_MAC $inmac8, OUT_MAC $outmac8 ,DELAY 0.2, LOSS 0.9)

port1->[0]router[0]->port1;
port2->[1]router[1]->port2;
port3->[2]router[2]->port3;
port4->[3]router[3]->port4;
port5->[4]router[4]->port5;
port6->[5]router[5]->port6;
port7->[6]router[6]->port7;
port8->[7]router[7]->port8;
