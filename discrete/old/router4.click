require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library ../elements/kn_router.click);

router::Kn_Router(NETADDRESS 11)

//port0::RouterPort(DEV veth2, IN_MAC 06:90:c5:e8:3d:22, OUT_MAC 3e:ce:b5:88:c8:81)
port0::RouterPort(DEV veth10, IN_MAC f6:de:f7:3a:d6:43 , OUT_MAC 3e:c9:49:43:23:56 )
port1::RouterPort(DEV veth3, IN_MAC 52:f5:79:2c:11:48, OUT_MAC da:b3:af:eb:af:ac)
port2::RouterPort(DEV veth5, IN_MAC 8e:94:56:8b:15:cc, OUT_MAC aa:b8:11:cb:90:9c)
port3::RouterPort(DEV veth7, IN_MAC ca:6d:3d:a1:6f:23, OUT_MAC fe:40:1c:54:64:b9)

port0->[0]router[0]->port0;
port1->[1]router[1]->port1;
port2->[2]router[2]->port2;
port3->[3]router[3]->port3;

