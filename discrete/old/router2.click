require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library ../elements/kn_router.click);

router::Kn_Router(NETADDRESS $netaddr)

port1::RouterPort(DEV $dev1, IN_MAC $inmac1, OUT_MAC $outmac1)
port2::RouterPort(DEV $dev2, IN_MAC $inmacmac2, OUT_MAC $outmac2)

port1
->[0]router[0]
->port1;

port2
->[1]router[1]
->port2;
