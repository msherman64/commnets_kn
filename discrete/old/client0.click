require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library ../elements/kn_client.click);

client::Kn_Client(NETADDRESS $netaddr)
port::RouterPort(DEV $dev1, IN_MAC $mac1, OUT_MAC $mac2)
port->[0]client[0]->port;
