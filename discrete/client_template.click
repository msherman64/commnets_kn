require(library /home/comnetsii/elements/routerport.click);
require(library /home/comnetsii/elements/lossyrouterport.click);
require(library /home/comnetsii/elements/kn_client.click);

//client::Kn_Client(NETADDRESS 1)
//client::Kn_Client(NETADDRESS 1, K 1, DEST1 2, PAYLOAD FFFFFF)
//client::Kn_Client(NETADDRESS 1, K 2, DEST1 2, DEST2 3, PAYLOAD FFFFFF)
client::Kn_Client(NETADDRESS 1, K 3, DEST1 2, DEST2 3, DEST3 4, PAYLOAD FFFFFF)

port::LossyRouterPort(, DELAY 0.2, LOSS 0.9)
port->[0]client[0]->port;

//Copy template as needed, uncomment only appropriate client line.
//Paste in device and address as needed
/*
DEV veth1, IN_MAC 8a:be:03:35:fe:ea, OUT_MAC ee:ea:b2:75:d8:f8
DEV veth2, IN_MAC ee:ea:b2:75:d8:f8, OUT_MAC 8a:be:03:35:fe:ea
DEV veth3, IN_MAC 46:2b:fa:be:49:bc, OUT_MAC 72:ac:0c:d6:ad:de
DEV veth4, IN_MAC 72:ac:0c:d6:ad:de, OUT_MAC 46:2b:fa:be:49:bc
DEV veth5, IN_MAC 3e:3f:2d:c7:6f:4f, OUT_MAC ba:95:e4:10:7b:d4
DEV veth6, IN_MAC ba:95:e4:10:7b:d4, OUT_MAC 3e:3f:2d:c7:6f:4f
DEV veth7, IN_MAC 82:fe:f8:0e:59:15, OUT_MAC fe:d7:b2:3d:ea:04
DEV veth8, IN_MAC fe:d7:b2:3d:ea:04, OUT_MAC 82:fe:f8:0e:59:15
DEV veth9, IN_MAC 7a:4f:1e:b3:6e:18, OUT_MAC a2:5b:53:1a:96:57
DEV veth10, IN_MAC a2:5b:53:1a:96:57, OUT_MAC 7a:4f:1e:b3:6e:18
DEV veth11, IN_MAC e6:2a:47:a6:b9:35, OUT_MAC 16:16:c1:4e:9f:e5
DEV veth12, IN_MAC 16:16:c1:4e:9f:e5, OUT_MAC e6:2a:47:a6:b9:35
DEV veth13, IN_MAC 42:83:c6:da:60:61, OUT_MAC 0e:dd:48:24:09:d9
DEV veth14, IN_MAC 0e:dd:48:24:09:d9, OUT_MAC 42:83:c6:da:60:61
DEV veth15, IN_MAC fa:8e:32:6d:ca:01, OUT_MAC 12:c9:84:12:d4:f0
DEV veth16, IN_MAC 12:c9:84:12:d4:f0, OUT_MAC fa:8e:32:6d:ca:01
DEV veth17, IN_MAC ca:e2:83:fa:2c:68, OUT_MAC 36:c5:5f:a6:0b:e8
DEV veth18, IN_MAC 36:c5:5f:a6:0b:e8, OUT_MAC ca:e2:83:fa:2c:68
DEV veth19, IN_MAC ae:94:62:12:d6:68, OUT_MAC d2:88:9a:63:46:33
DEV veth20, IN_MAC d2:88:9a:63:46:33, OUT_MAC ae:94:62:12:d6:68
*/
