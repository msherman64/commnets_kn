Comnets II Project 4: K/N Multicast Routing
Group 1
Members: Michael Sherman, Murtada Aleer, Leonard Park

Folder Structure:
Source files are in the main directory, commnets_kn
test files are in the directory test
compount elements are in the directory elements
Click configurations for the demo are in the folder Discrete

Before running, execute the script ./compile.sh in the main directory.
this will copy the files into the click and elements directories, and run the relevant scripts.

To Run:
There are two templates, router_template and client_template
Copy each for as many elements as needed, then remove extra ports

Copy DEV info from the block in bottom of the file, mac addresses may need to be updated.

To run each, for example, execute sudo ~/click/userlevel/click router8.click

The router is composed of the following elements:

knarq
knclassifier
knroute
knupdate
knforward
knswitch
routerports

The client is composed of the following elements

knsource(hello)
knsource(data)
knswitch
knarq
routerport
print

The flow is visible in the compound elements
kn_router.click
kn_client.click

Sub Elements

Source (0/1) Push
    Filename: KnSource
    The source element can create any of the 4 packet types, via provided configuration file. It does so on a 5 second timer.

ARQ (2/2) (1 push input, 1 pull input, 2 push outputs)
    Filename: KnARQ
    The ARQ element provides reliability for each routerport. It receives messages from the routerport on a push basis, and pulls from the switch on a task and timer.
    The task polls for new packets in the queue, and the timer is used for resending messages on timeout.
    When an appropriate ACK is received, a flag is set to true, and the task can pull the next packet, if available.
    Although unspecified in the spec, the sequence number is 1 byte unsigned, from 0-255. It increments by one on each successful message sent, and wraps safely.
    The ARQ element will forward any packet type except ACKs.

Classifier (1-N/2) Push
    Filename: KnClassifier
    The classifier takes in any non-ACK packet, and annotates it with the incoming port number.
    Hello and Update messages are sent to output 0, and Data messages are sent to output 1.
    It will drop the packet and send an error message on invalid type.

Switch (2/1-N) Push
    Filename: KnSwitch
    The switch takes packets from the update sender and the forwarder.
    It reads the output port annotation anno_u8(2), and sends to the appropriate port.
    If the output annotation is 255, it will copy the packet and send to all output ports. This is used for the distribution of hello/update messages

Routing (2/0) Push
    Filename: KnRoute
    The routing element takes both hello and update packets, and updates both the neighbor and routing tables.
    It uses the same logic on both packet types, due to similarity of information.
    For each packet: It adds the source address and port to the neighbor table.
    For each packet: It adds the source address and cost 1 to the routing table, with nexthop equal to the source address. This is done by adding to the vector described below.
    If the packet is of type update, and has a nonzero length field:
    It parses each destination-cost pair into a vector of structs.
    Iterating across the vector, each item is added to the routing table, according to the following rules.
        If the destination is not in the table, add.
        If the route has a lower cost, add.
        If the route has an equal cost, but is from a new nexthop, add, up to 3 routes.
    The routing table itself is a Hashtable<destination_address,struct<rt_val>>
    The router adds its own address to the table with cost 0, to prevent looping routes.
    The routing and neighbor tables are accessable with public function Get_Rtable(), and Get_Ntable().

Update Sender (0/1) Push
    Filename: KnUpdate
    The update sender reads the routing table from KnRoute. For each destination in the routing table, it adds the pair (destination,cost) to the update packet.
    If the routing table is empty, it will send an empty update packet, with length field 0.
    The packet has its output port annotated with 255, for broadcast.

Forwarding (1/1) Push
    Filename: KnForward
    The forwarding element calls both the Get_Rtable and GetNtable methods from KnRoute.
    It uses the following logic:
    For each nonzero destination field
    Choose the K destinations with lowest cost.
        Of those destinations, get the list of nexthops.
        For all nexthops chosen, find the most shared nexthop.
        Forward packet to chosen nexthop.
        Remove matched destinations from list and repeat, copying packet.
    The multicast forwarding table is thus built on the fly for each packet, as it is infeasible to store a table for all possible multicast combinations.

