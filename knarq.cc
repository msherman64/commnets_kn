#include <click/config.h>
#include <click/args.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/standard/scheduleinfo.hh>
#include "knarq.hh"
#include "kn_packets.hh"
CLICK_DECLS
//addedcomment
KnArq::KnArq() : _timer(this),_pulltask(this) {}
KnArq::~KnArq(){}

int KnArq::configure(Vector<String> &conf, ErrorHandler *errh) {
    return Args(conf, this, errh)
            .read_m("TIMEOUT", _timeout)
            .complete();
} 

int KnArq::initialize(ErrorHandler *errh) {
	_timer.initialize(this);
    ScheduleInfo::initialize_task(this, &_pulltask, errh); //attach task to this element
    _last_recv_seqnum=254;
    _last_msg_sent=0;
    _acked=true;
	return 0;
}

void KnArq::run_timer(Timer *timer){
//    click_chatter("timer fired");
    send_message();
}

bool KnArq::run_task(Task *){
    if(_acked==true){
        if(pull_message()){ //pull next message in queue from switchqueue
            send_message();
//            click_chatter("sent new message");
            _pulltask.fast_reschedule();
            return true;
        }
    }
    _pulltask.fast_reschedule();
    return false;
}

void KnArq::push(int port, Packet *p) { //fires when push received from routerport
   	hello_packet_format *format = (hello_packet_format *)p->data(); //cast packet to struct
    int type = format->type;
//    click_chatter("received packet on port %d with type %d",port,type);
    switch(port){
        case 0 : 
            if(type==3){  //received ack, increment sequence number and pull next message to send 
//                click_chatter("running ack handler");
                ack_handler(p);
            }
            else{ //received packet other than ack, reply with ack
                send_ack(p); //output 0 push ack
                message_handler(p);
            }
            break;
        case 1 : p->kill(); //push from switch, error
                 click_chatter("error, switch should not push");
                 break;
        default : //invalid port
                 break;
    }
}

bool KnArq::pull_message(){
    _qpacket = input(1).pull(); //get next message from switchqueue
    if(_qpacket){
        _last_msg_sent++;
        hello_packet_format *format = (hello_packet_format *)_qpacket->data(); //cast packet to struct
        format->seqnum=_last_msg_sent; //set seqnum for next packet to send
        return true;
    }
    else
        return false;
}

void KnArq::send_message(){
    Packet* sendpkt = _qpacket->clone();
    output(0).push(sendpkt);
    _acked=false;
    _timer.reschedule_after_sec(_timeout);
}

void KnArq::ack_handler(Packet *p){
   	ack_packet_format *ack_format = (ack_packet_format *)p->data(); //cast message packet to struct
//    click_chatter("received ack with sequence number %d, last sent was %d",ack_format->seqnum,_last_msg_sent);
    if(ack_format->seqnum==_last_msg_sent){
        _acked=true;
        _timer.unschedule();
//        click_chatter("acked set to true, timer unscheduled");
    }
    p->kill();
}

void KnArq::send_ack(Packet *p){
   	hello_packet_format *hello_format = (hello_packet_format *)p->data(); //cast message packet to struct
    WritablePacket *ack_p = Packet::make(0, 0, sizeof(ack_packet_format), 0); //make packet with payload
   	ack_packet_format *ack_format = (ack_packet_format *)ack_p->data(); //cast packet to struct
    ack_format->type=3;
    ack_format->seqnum=hello_format->seqnum; //link local sequence number
    ack_format->source=0; //addresses not needed for ack
    ack_format->dest=0;
    output(0).push(ack_p);
}

void KnArq::message_handler(Packet *p){
   	hello_packet_format *hello_format = (hello_packet_format *)p->data(); //cast message packet to struct
    uint8_t recv_seqnum = hello_format->seqnum;
    if(recv_seqnum==_last_recv_seqnum){
        p->kill(); //duplicate packet received
    }
    else{
        _last_recv_seqnum=recv_seqnum;
        output(1).push(p);
    }
}



CLICK_ENDDECLS
EXPORT_ELEMENT(KnArq)
