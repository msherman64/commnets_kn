#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet_anno.hh>
#include "knforward.hh"
#include "kn_packets.hh"
#include <algorithm>
#include <vector>
CLICK_DECLS

KnForward::KnForward(){}
KnForward::~KnForward(){}

int KnForward::configure(Vector<String> &conf, ErrorHandler *errh) {

    String ename;

    return Args(conf, this, errh)
        .read_m("ROUTING", _ename)
        .complete();
}

int KnForward::initialize(ErrorHandler *errh) {
    int rtret=1;
    if(Element* e = cp_element(_ename, this, errh)){
        //        click_chatter("found routing table element");
        _rt_elem = (KnRoute *)(e); //cast element to router type
        rtret=0; //indicate success
    }
    return rtret;
}

void KnForward::push(int port, Packet *p) {
    _packet = p;
    data_packet_format *data = (data_packet_format *)(p->data()); //cast packet
    if(data->type !=4){
        p->kill();
        click_chatter("not data packet, classifier failed");
    }
    else
    {
        _rtable = _rt_elem->get_rtable(); //get updated routing table
        std::vector<KnRoute::rt_val> picked_addr = choose_dests(p); //pick k dests
        choose_most_shared(picked_addr); //choose nexthops and send
    }
}

//uint8_t KnForward::get_cost(uint16_t dest_addr){
//    _rtable = _rt_elem->get_rtable();
//    KnRoute::rt_val itrval = _rtable.get(dest_addr);
//    return itrval.cost;
//}

struct less_than_key {
    inline bool operator() (const KnRoute::rt_val& d1,const KnRoute::rt_val& d2){
        return d1.cost < d2.cost;
    }
};


std::vector<KnRoute::rt_val> KnForward::choose_dests(Packet *p){
    data_packet_format *data = (data_packet_format *)(p->data()); //cast packet
    std::vector<KnRoute::rt_val> dest_vec;
    if(data->dest1){ //make array of destinations
        dest_vec.push_back(_rtable.get(data->dest1));}
    if(data->dest2){
        dest_vec.push_back(_rtable.get(data->dest2));}
    if(data->dest3){
        dest_vec.push_back(_rtable.get(data->dest3));}

    std::sort(dest_vec.begin(),dest_vec.end(),less_than_key()); //sort destinations by cost

    uint8_t k = data->k;
    std::vector<KnRoute::rt_val> out_vec(dest_vec.begin(),dest_vec.begin()+k); //get k cheapest

    return out_vec;
}


void KnForward::choose_most_shared(std::vector<KnRoute::rt_val> in_vec){
    HashTable<uint16_t,int> fwd_table;
    int counter = 0; 
    for(std::vector<KnRoute::rt_val>::iterator i = in_vec.begin(); i!=in_vec.end(); ++i)
    {
        if(i->nexthop){ //make array of destinations
            counter = fwd_table.get(i->nexthop);
            counter++;
            fwd_table.set(i->nexthop,counter);
        }
        if(i->nexthop2){
            counter = fwd_table.get(i->nexthop2);
            counter++;
            fwd_table.set(i->nexthop2,counter);
        }
        if(i->nexthop3){
            counter = fwd_table.get(i->nexthop3);
            counter++;
            fwd_table.set(i->nexthop3,counter);
        }
    }
    uint8_t max = 0;
    uint16_t chosen = 0;
    for(HashTable<uint16_t,int>::iterator i = fwd_table.begin();i != fwd_table.end(); ++i){
        if(i.value() > max){
            max=i.value();
            chosen=i.key();
        }
    }

    click_chatter("max=%d,%d destinations",max,in_vec.size());
    if(max)
    {
        uint8_t port = read_neighbor_table(chosen);
        click_chatter("FORWARDING %d to port %d",chosen, port);
        Packet* c_p = _packet->clone();
        WritablePacket* out_p = c_p->uniqueify();
    //    click_chatter("UNIQUEIFY");
        data_packet_format *data = (data_packet_format *)(out_p->data()); //cast packet
      //  click_chatter("CAST DATA");
        data->k=max;
       // click_chatter("SET K");
        out_p->set_anno_u8(2,port); //sets output port
       // click_chatter("SET ANNO");
        output(0).push(out_p);      //forwards packet to switch
       // click_chatter("PUSH SUCCESSFUL");

        if(max==in_vec.size()){   // if nexthop is valid for all K destinations
        }
        else
        {
            click_chatter("port not shared, max=%d",max);
            //make list of remainder
            for(int i = in_vec.size()-1; i>=0; i--)
            {
                if(in_vec.at(i).nexthop==chosen){ //remove destinations matching chosen
                    in_vec.erase(in_vec.begin()+i);
                }
                else if(in_vec.at(i).nexthop2==chosen){ 
                    in_vec.erase(in_vec.begin()+i);
                }
                else if(in_vec.at(i).nexthop3==chosen){
                    in_vec.erase(in_vec.begin()+i);
                }
            }
            KnForward::choose_most_shared(in_vec);
        }
    }

}


//uint16_t KnForward::read_routing_table(uint16_t dest_addr, int i){
//    _rtable = _rt_elem->get_rtable();
//    int size = _rtable.size();
//    if(size){
//        KnRoute::rt_val itrval = _rtable.get(dest_addr);
//        uint16_t nexthop = 0;
//        switch(i){
//            case 0 : nexthop = itrval.nexthop;
//                     break;
//            case 1 : nexthop = itrval.nexthop2;
//                     break;
//            case 2 : nexthop = itrval.nexthop3;
//                     break;
//        }
//        click_chatter("FORWARD packet with dest %d has nexthop %d with cost %d",dest_addr,nexthop,itrval.cost);
//        return nexthop;
//    }
//    else{
//        return 0; //return 0 if table empty
//    }
//}

//void KnForward::build_fwd_table(Packet *p){
//    data_packet_format *data = (data_packet_format *)(p->data()); //cast packet
//    HashTable<uint16_t,int> fwd_table;
//    uint16_t destarr[3];
//    destarr[0] = data->dest1;
//    destarr[1] = data->dest2;
//    destarr[2] = data->dest3;
//   
//    for(int j=0; j<2; j++){ //iterate over potential nexthops
//        for(int i=0; i<2; i++){//iterate over destinations
//            uint16_t t_nexthop = read_routing_table(destarr[i],j);//get nexthop #j from dest i
//            int counter = fwd_table.get(t_nexthop); //get current counter val
//            counter++; //increment counter
//            fwd_table.set(t_nexthop,counter); //add new index to forwarding table
//        }
//    }
//}

uint8_t KnForward::read_neighbor_table(uint16_t nexthop){

    _ntable = _rt_elem->get_ntable();
    //    int size = _ntable.size();

    uint8_t port = _ntable.get(nexthop);
    //    click_chatter("FORWARD packet with nexthop %d has output port %d",nexthop,port);
    return port;
}


//void KnForward::read_table(){
//    _rtable = _rt_elem->get_rtable();
//    int size = _rtable.size();
//    click_chatter("table has %d addresses \n", size);
//
//    Update_Payload payload[size]; //initialize payload to num rows
//
//    if(size){ //if has entries
//        int i=0; //init array idex
//        for( KnRoute::Rtable::iterator rtable_it = _rtable.begin();
//                rtable_it != _rtable.end();
//                rtable_it++)
//        {
//            uint16_t addr = rtable_it.key();
//            KnRoute::rt_val itrval = rtable_it.value();
//            click_chatter("address %d has cost %d and nexthop %d",addr,itrval.cost,itrval.nexthop);
//            //update packet payload is pairs of addr(uint16_t),cost(uint8_t)
//            payload[i] = {addr,itrval.cost}; //write table entry into payload
//            i++;
//        }
//    }
//
//    uint16_t payload_size = sizeof(payload);
//    click_chatter("payload size %d",payload_size);
//
//    WritablePacket *p = Packet::make(sizeof(update_packet_format),payload,payload_size,0);
//    WritablePacket *p_header = p->push(sizeof(update_packet_format));
//    update_packet_format *update = (update_packet_format *)(p_header->data()); //cast packet
//    update->type=2;
//    update->source=_address;
//    update->seqnum=0;
//    update->payload_length = payload_size;
//
//    p_header->set_anno_u8(2, 255); //let 255 or FF mean all ports
//
//    output(0).push(p_header);
//
//}
//



    CLICK_ENDDECLS
EXPORT_ELEMENT(KnForward)

