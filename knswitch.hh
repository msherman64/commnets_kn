#ifndef CLICK_KNSWITCH_HH
#define CLICK_KNSWITCH_HH

#include <click/element.hh>

CLICK_DECLS

class KnSwitch : public Element {

public:
         KnSwitch();
	~KnSwitch();
	
	const char *class_name() const { return "KnSwitch";}
	const char *port_count() const { return "2/1-"; }
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);

	void push(int, Packet *);

};

CLICK_ENDDECLS

#endif

