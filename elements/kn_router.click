//
// Compound class that provide  abstraction for a router
//

elementclass Kn_Router { NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq::KnArq(TIMEOUT 1)

input[0]->[0]arq[1]->[0]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]-> Queue()->[1]arq[0]-> [0]output;

||

NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)

input[0]->[0]arq1[1]->[0]classifier;
input[1]->[0]arq2[1]->[1]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]->Queue()->[1]arq1[0]->[0]output;
switch[1]->Queue()->[1]arq2[0]->[1]output;

||

NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)
arq3::KnArq(TIMEOUT 1)

input[0]->[0]arq1[1]->[0]classifier;
input[1]->[0]arq2[1]->[1]classifier;
input[2]->[0]arq3[1]->[2]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]->Queue()->[1]arq1[0]->[0]output;
switch[1]->Queue()->[1]arq2[0]->[1]output;
switch[2]->Queue()->[1]arq3[0]->[2]output;

||

NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)
arq3::KnArq(TIMEOUT 1)
arq4::KnArq(TIMEOUT 1)

input[0]->[0]arq1[1]->[0]classifier;
input[1]->[0]arq2[1]->[1]classifier;
input[2]->[0]arq3[1]->[2]classifier;
input[3]->[0]arq4[1]->[3]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]->Queue()->[1]arq1[0]->[0]output;
switch[1]->Queue()->[1]arq2[0]->[1]output;
switch[2]->Queue()->[1]arq3[0]->[2]output;
switch[3]->Queue()->[1]arq4[0]->[3]output;

||

NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)
arq3::KnArq(TIMEOUT 1)
arq4::KnArq(TIMEOUT 1)
arq5::KnArq(TIMEOUT 1)

input[0]->[0]arq1[1]->[0]classifier;
input[1]->[0]arq2[1]->[1]classifier;
input[2]->[0]arq3[1]->[2]classifier;
input[3]->[0]arq4[1]->[3]classifier;
input[4]->[0]arq5[1]->[4]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]->Queue()->[1]arq1[0]->[0]output;
switch[1]->Queue()->[1]arq2[0]->[1]output;
switch[2]->Queue()->[1]arq3[0]->[2]output;
switch[3]->Queue()->[1]arq4[0]->[3]output;
switch[4]->Queue()->[1]arq5[0]->[4]output;


||

NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)
arq3::KnArq(TIMEOUT 1)
arq4::KnArq(TIMEOUT 1)
arq5::KnArq(TIMEOUT 1)
arq6::KnArq(TIMEOUT 1)

input[0]->[0]arq1[1]->[0]classifier;
input[1]->[0]arq2[1]->[1]classifier;
input[2]->[0]arq3[1]->[2]classifier;
input[3]->[0]arq4[1]->[3]classifier;
input[4]->[0]arq5[1]->[4]classifier;
input[5]->[0]arq6[1]->[5]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]->Queue()->[1]arq1[0]->[0]output;
switch[1]->Queue()->[1]arq2[0]->[1]output;
switch[2]->Queue()->[1]arq3[0]->[2]output;
switch[3]->Queue()->[1]arq4[0]->[3]output;
switch[4]->Queue()->[1]arq5[0]->[4]output;
switch[5]->Queue()->[1]arq6[0]->[5]output;

||

NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)
arq3::KnArq(TIMEOUT 1)
arq4::KnArq(TIMEOUT 1)
arq5::KnArq(TIMEOUT 1)
arq6::KnArq(TIMEOUT 1)
arq7::KnArq(TIMEOUT 1)

input[0]->[0]arq1[1]->[0]classifier;
input[1]->[0]arq2[1]->[1]classifier;
input[2]->[0]arq3[1]->[2]classifier;
input[3]->[0]arq4[1]->[3]classifier;
input[4]->[0]arq5[1]->[4]classifier;
input[5]->[0]arq6[1]->[5]classifier;
input[6]->[0]arq7[1]->[6]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]->Queue()->[1]arq1[0]->[0]output;
switch[1]->Queue()->[1]arq2[0]->[1]output;
switch[2]->Queue()->[1]arq3[0]->[2]output;
switch[3]->Queue()->[1]arq4[0]->[3]output;
switch[4]->Queue()->[1]arq5[0]->[4]output;
switch[5]->Queue()->[1]arq6[0]->[5]output;
switch[6]->Queue()->[1]arq7[0]->[6]output;

||

NETADDRESS $address |

switch::KnSwitch()
classifier::KnClassifier()
route::KnRoute(ADDRESS $address)
update::KnUpdate(ADDRESS $address, TIMEOUT 5, ROUTING route)
forward::KnForward(ROUTING route)
arq1::KnArq(TIMEOUT 1)
arq2::KnArq(TIMEOUT 1)
arq3::KnArq(TIMEOUT 1)
arq4::KnArq(TIMEOUT 1)
arq5::KnArq(TIMEOUT 1)
arq6::KnArq(TIMEOUT 1)
arq7::KnArq(TIMEOUT 1)
arq8::KnArq(TIMEOUT 1)

input[0]->[0]arq1[1]->[0]classifier;
input[1]->[0]arq2[1]->[1]classifier;
input[2]->[0]arq3[1]->[2]classifier;
input[3]->[0]arq4[1]->[3]classifier;
input[4]->[0]arq5[1]->[4]classifier;
input[5]->[0]arq6[1]->[5]classifier;
input[6]->[0]arq7[1]->[6]classifier;
input[7]->[0]arq8[1]->[7]classifier;

classifier[0]-> route;
classifier[1]->forward->[1]switch;
update-> [0]switch;

switch[0]->Queue()->[1]arq1[0]->[0]output;
switch[1]->Queue()->[1]arq2[0]->[1]output;
switch[2]->Queue()->[1]arq3[0]->[2]output;
switch[3]->Queue()->[1]arq4[0]->[3]output;
switch[4]->Queue()->[1]arq5[0]->[4]output;
switch[5]->Queue()->[1]arq6[0]->[5]output;
switch[6]->Queue()->[1]arq7[0]->[6]output;
switch[7]->Queue()->[1]arq8[0]->[7]output;
}
