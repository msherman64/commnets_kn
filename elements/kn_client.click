//
// Compound class that provide  abstraction for a router
//

elementclass Kn_Client { 

NETADDRESS $address |
switch::KnSwitch()
hello::KnSource(TYPE 1, SOURCE $address, SEQNUM 0)
arq::KnArq(TIMEOUT 1)
input[0]->[0]arq[1]->Print("Client Received Message")->Discard;
hello-> [0]switch;
Idle()->[1]switch;
switch[0]->Queue()->[1]arq[0]->[0]output;

||
NETADDRESS $address, K $k, DEST1 $dest1, PAYLOAD $payload |
switch::KnSwitch()
hello::KnSource(TYPE 1, SOURCE $address, SEQNUM 0)
data::KnSource(TYPE 4, SOURCE $address, SEQNUM 0, K $k, DEST1 $dest1, PAYLOAD $payload)
arq::KnArq(TIMEOUT 1)
input[0]->[0]arq[1]->Print("Client Received Message")->Discard;
hello-> [0]switch;
data->[1]switch;
switch[0]->Queue()->[1]arq[0]->[0]output;

||

NETADDRESS $address, K $k, DEST1 $dest1, DEST2 $dest2, PAYLOAD $payload |
switch::KnSwitch()
hello::KnSource(TYPE 1, SOURCE $address, SEQNUM 0)
data::KnSource(TYPE 4, SOURCE $address, SEQNUM 0, K $k, DEST1 $dest1, DEST2 $dest2, PAYLOAD $payload)
arq::KnArq(TIMEOUT 1)
input[0]->[0]arq[1]->Print("Client Received Message")->Discard;
hello-> [0]switch;
data->[1]switch;
switch[0]->Queue()->[1]arq[0]->[0]output;

||

NETADDRESS $address, K $k, DEST1 $dest1, DEST2 $dest2, DEST3 $dest3, PAYLOAD $payload |
switch::KnSwitch()
hello::KnSource(TYPE 1, SOURCE $address, SEQNUM 0)
data::KnSource(TYPE 4, SOURCE $address, SEQNUM 0, K $k, DEST1 $dest1, DEST2 $dest2, DEST3 $dest3, PAYLOAD $payload)
arq::KnArq(TIMEOUT 1)
input[0]->[0]arq[1]->Print("Client Received Message")->Discard;
hello-> [0]switch;
data->[1]switch;
switch[0]->Queue()->[1]arq[0]->[0]output;

}
