#ifndef CLICK_PACKETHEADER_HH
#define CLICK_PACKETHEADER_HH

#include <string>
// matching spec as of 4/23, 4pm

/* HELLO PACKET (type 1)*/
/* Length to be 0 for HELLO PACKET */
#pragma pack(push,1)
typedef struct {
    uint8_t type;
    uint16_t source;
    uint8_t seqnum;
 
} hello_packet_format;
#pragma pack(pop)


/* HELLO/UPDATE PACKET (type 2)*/
/* Length to be 0 for HELLO PACKET */

#pragma pack(push,1)
typedef struct {
    uint8_t type;
    uint16_t source;
    uint8_t seqnum;
    uint16_t payload_length;
 
} update_packet_format;
#pragma pack(pop)


/* ACK packet (type 3) */

#pragma pack(push,1)
typedef struct {
    uint8_t type;
    uint16_t source;
    uint8_t seqnum;
    uint16_t  dest;
    
} ack_packet_format;
#pragma pack(pop)


/* DATA PACKET (type 4)*/

#pragma pack(push,1)
typedef struct {
    uint8_t type;
    uint16_t source;
    uint8_t seqnum;
    uint8_t k;
    uint16_t  dest1;
    uint16_t  dest2;
    uint16_t  dest3;
    uint16_t payload_length;
 
} data_packet_format;
#pragma pack(pop)
#endif

