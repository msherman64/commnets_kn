#ifndef CLICK_KNFORWARD_HH
#define CLICK_KNFORWARD_HH

#include <click/element.hh>
#include <click/timer.hh>
#include <click/hashtable.hh>
#include <click/list.hh>
#include "knroute.hh"
#include <vector>
CLICK_DECLS

class KnForward : public Element {

    public:
        KnForward();
        ~KnForward();

        const char *class_name() const { return "KnForward";}
        const char *port_count() const { return "1/1"; }
        const char *processing() const { return PUSH; }

        int configure(Vector<String>&, ErrorHandler*);
        int initialize(ErrorHandler*);


        //    void run_timer(Timer *);

        //#pragma pack(push,1)
        //struct Update_Payload
        //    {
        //        uint16_t address;
        //        uint8_t cost;
        //    };
        //#pragma pack(pop)

        void push(int, Packet *);
//        uint16_t read_routing_table(uint16_t , int);
        uint8_t read_neighbor_table(uint16_t );
//        void build_fwd_table(Packet *);
        std::vector<KnRoute::rt_val> choose_dests(Packet *);
        void choose_most_shared(std::vector<KnRoute::rt_val>);
//        uint8_t get_cost(uint16_t );
        bool sort_cost(uint16_t & , uint16_t & );

    private:
        String _ename;
        //    Timer _timer;
        //    uint8_t _timeout;
        //    uint16_t _address;
        Packet* _packet;

        KnRoute * _rt_elem;
        KnRoute::Ntable _ntable;
        KnRoute::Rtable _rtable;

};

CLICK_ENDDECLS

#endif

